Slim::Engine.set_options shortcut: {
	'#' => {attr: 'id'},
	'.' => {attr: 'class'},
	'&' => {attr: 'type'},
	'@' => {attr: 'role'},
	'%' => {attr: 'data-toggle'}
}