class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.references :food_category, index: true
      t.string :food_id
      t.string :name
      t.string :desc
      t.decimal :smprice
      t.decimal :lgprice
      t.boolean :is_spicy
    end
  end
end
