class CreateOpenTimes < ActiveRecord::Migration
  def change
    create_table :open_times do |t|
      t.string :day_of_week
      t.time :open_time
      t.time :closed_time
    end
  end
end
