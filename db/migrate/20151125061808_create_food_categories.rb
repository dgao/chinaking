class CreateFoodCategories < ActiveRecord::Migration
  def change
    create_table :food_categories do |t|
      t.string :category
      t.string :extra
      t.string :desc
      t.string :color
      t.string :anchor
    end
  end
end
