# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "page:change", ->
  # Navigates to anchor
  $('a[href^="#"]').on 'click', (event) ->
    # Prevent page from redirecting
    event.preventDefault()
    text = $(this).attr('href')
    target = $(text)
    if text == "#showall"
      $('h3.jumbotron').next().removeClass 'hidden'
    else if text == "#hideall"
      $('h3.jumbotron').next().addClass 'hidden'
    else if target.length
      $('html, body').animate { scrollTop: target.offset().top - 50 }, 300
    return
  # Shows/hides the list for each category
  $('h3.jumbotron').on 'click', ->
    $(this).next().toggleClass 'hidden'
    return
  # Modal view when clicking on row (item)
  $('table.table > tbody > tr').not('.sm-lg').on 'click', (event) ->
    # Removes text selection
    # window.getSelection().removeAllRanges()

    # If already selected, de-select it
    if $(this).hasClass 'success'
      # Prevent modal from showing
      event.stopPropagation()
      $(this).removeClass 'success'
      # If none selected, remove Order button
      if !$('.success').length
        $('.checkout').addClass 'hidden'
    else
      # Memorize the row we clicked in DOM, will reference later
      # Better than using global variables - prevents memory leak
      $('#food-item').data('row', this)
      title = $(this).children('td:eq(1)').html().split('<')[0]
      $('.modal-title').html(title)
      # We need choices if there is an 'or'
      choice1 = title.split(' or ')[0]
      choice2 = title.split(' or ')[1]
      # If a second choice exists
      if choice2
        # Remove the hidden class so we can choose
        $('.choice').removeClass('hidden')
        # Populate radio buttons with choices
        $("label[for='food_name_0']").contents().last().replaceWith(choice1)
        $("label[for='food_name_1']").contents().last().replaceWith(choice2)
      else
        $('.choice').addClass('hidden')
      return
    return
  # When custom order is complete
  $('.done').on 'click', (event) ->
    if !$('#food_quantity').val()
      # Highlight errors
      $('#food_quantity').closest('.form-group').addClass('has-error')
      # Stop submission of the form
      event.stopPropagation()
    else
      # Highlights row green and shows Order button
      $('#food_quantity').closest('.form-group').removeClass('has-error')
      $($('#food-item').data('row')).addClass 'success'
      $('.checkout').removeClass 'hidden'
      $('#new_food')[0].reset()
    return
  return