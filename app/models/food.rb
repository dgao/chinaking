class Food < ActiveRecord::Base
	belongs_to :food_category, inverse_of: :foods
	attr_accessor :quantity, :request
	validates_presence_of :quantity, :name
end
