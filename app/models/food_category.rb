class FoodCategory < ActiveRecord::Base
	has_many :foods, inverse_of: :food_category, dependent: :destroy
end
