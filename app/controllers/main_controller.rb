class MainController < ApplicationController
  def index
    @food_categories = FoodCategory.all
    @announcement = ""
    @times = OpenTime.all
    @food = Food.new
  end

  def create
    @food = Food.new(params[:food])
 
    respond_to do |format|
      if @food.save
        puts "hi"
        redirect_to @food
        #render @food
      else
        puts "bye"
        render action: "new"
      end
    end
  end
end
